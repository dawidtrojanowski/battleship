#ifndef SETSHIPS_H
#define SETSHIPS_H

#include <QDialog>

namespace Ui {
class SetShips;
}

class SetShips : public QDialog
{
    Q_OBJECT

public:
    explicit SetShips(QWidget *parent = nullptr);
    ~SetShips();

private:
    Ui::SetShips *ui;
};

#endif // SETSHIPS_H
