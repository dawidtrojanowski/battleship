#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QPushButton>
#include <QRadioButton>
#include <QStringList>
#include <QLabel>
#include <QMessageBox>
#include "clientstuff.h"
#include <iostream>

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = nullptr);
    QPushButton *allButtons[10][10];
    QPushButton *prepareButtons[10][10];
    QPushButton *enemyButtons[10][10];
    QString board[10][10];
    QString miss = "miss";
    QString hit = "hit";
    QString shipOrientation;
    QString shipCoords = "";
    QString serverMessage = "";
    QString allShipsPositions = "";
    int playerScore = 0;
    int opponentScore = 0;
    int shipSize;
    int countSize;
    int previousX;
    int previousY;
    int ship4Count = 1;
    int ship3Count = 1;
    int ship2Count = 1;
    int ship1Count = 1;
    int clickedButtons = 0;
    int amountOfShips = 0;


    void resetGame();
    void setButtons();
    void establishConnection();
    bool ableToSetShip(int, int);
    bool checkRadioClicked();
    void clearBoard();
    bool didPlayerMiss();
    void disableUncheckedRadioButtons();
    void enableRadioButtons();
    void lockNeighbourFields();
    void lockNeighbourFieldsVertical(QStringList, QString, QString);
    void lockNeighbourFieldsHorizontal(QStringList, QString, QString);
    bool checkAmountOfShips();
    void addShipCount();
    void cancelShipSetting();
    void sendMessage(QString);
    void clearAllBoard();
    ~MainWindow();

public slots:
    void buttonPressed();
    void attackButtonPressed();
    void myButtonPressed();
    void setStatus(bool newStatus);
    void receivedSomething(QString msg);
    void gotError(QAbstractSocket::SocketError err);

private slots:
    void on_radioButton4_clicked();

    void on_radioButton3_clicked();

    void on_radioButton2_clicked();

    void on_radioButton1_clicked();

    void on_verticalRadioButton_clicked();

    void on_horizontalRadioButton_clicked();

    void on_cancelPushButton_clicked();

    void on_startGamePushButton_clicked();

    void on_pushButton_clicked();

    void on_clearAllButton_clicked();

private:
    Ui::MainWindow *ui;
    ClientStuff *client = NULL;
};

#endif // MAINWINDOW_H
