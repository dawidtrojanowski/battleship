#include "mainwindow.h"
#include "ui_mainwindow.h"

using namespace std;

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    clearBoard();
    setButtons();
}

MainWindow::~MainWindow()
{
    delete client;
    delete ui;
}

void MainWindow::setStatus(bool newStatus)
{
    if(newStatus) {
        MainWindow::findChild<QStackedWidget *>("stackedWidget")->setCurrentIndex(1);
        for (int i = 0; i < 10; i++)
            for (int j = 0; j < 10; j++)
                enemyButtons[i][j]->setEnabled(true);
    }
}

void MainWindow::setButtons() {

    for (int i = 0; i < 10; i++){
        for (int j = 0; j < 10; j++){
            QString buttonName = "pushButton" + QString::number(i) + QString::number(j);
            allButtons[i][j] = MainWindow::findChild<QPushButton *>(buttonName);
            connect(allButtons[i][j], SIGNAL(released()), this, SLOT(myButtonPressed()));
        }
    }

    for (int i = 0; i < 10; i++){
        for (int j = 0; j < 10; j++){
            QString buttonName = "enemyButton" + QString::number(i) + QString::number(j);
            enemyButtons[i][j] = MainWindow::findChild<QPushButton *>(buttonName);
            connect(enemyButtons[i][j], SIGNAL(released()), this, SLOT(attackButtonPressed()));
        }
    }

    for (int i = 0; i < 10; i++){
        for (int j = 0; j < 10; j++){
            QString buttonName = "setButton" + QString::number(i) + QString::number(j);
            prepareButtons[i][j] = MainWindow::findChild<QPushButton *>(buttonName);
            connect(prepareButtons[i][j], SIGNAL(released()), this, SLOT(buttonPressed()));
        }
    }

}

void MainWindow::establishConnection() {
    client = new ClientStuff("localhost", 6547);

    setStatus(client->getStatus());

    connect(client, &ClientStuff::hasReadSome, this, &MainWindow::receivedSomething);
    connect(client, &ClientStuff::statusChanged, this, &MainWindow::setStatus);
    // FIXME change this connection to the new syntax
    connect(client->tcpSocket, SIGNAL(error(QAbstractSocket::SocketError)),
            this, SLOT(gotError(QAbstractSocket::SocketError)));
}

void MainWindow::buttonPressed() {

    QPushButton *button = (QPushButton *)sender();
    if(checkRadioClicked()) {
        if (checkAmountOfShips()) {
            int row = button->objectName().right(1).toInt();
            int column = button->objectName().right(2).left(1).toInt();
            if (ableToSetShip(column, row)) {
                button->setStyleSheet("background-color: red; border: 1px solid black;");
                board[column][row] = "set";
                allButtons[column][row]->setStyleSheet("background-color: red; border: 1px solid black;");
                button->setEnabled(false);
                clickedButtons += 1;
                if (countSize == 0) {
                    amountOfShips += 1;
                    lockNeighbourFields();
                    clickedButtons = 0;
                    enableRadioButtons();
                }
            }
        }
    }
}

void MainWindow::attackButtonPressed() {

    QPushButton *button = (QPushButton *)sender();
    QString row = button->objectName().right(1);
    QString column = button->objectName().right(2).left(1);
    sendMessage("hit" + column + row);
}



bool MainWindow::ableToSetShip(int x, int y) {

    if (board[x][y] == "") {
        if (countSize == shipSize) {
            shipCoords = QString::number(x) + QString::number(y);
            previousX = x;
            previousY = y;
            countSize -= 1;
            disableUncheckedRadioButtons();
            return true;
        } else if (countSize > 0) {
            if (shipOrientation == "vertical")  {
                if ((previousX - x == 1 || previousX - x == -1) && (previousY - y == 0)) {
                    shipCoords += ":" + QString::number(x) + QString::number(y);
                    previousX = x;
                    previousY = y;
                    countSize -= 1;
                    return true;
                }
            } else if (shipOrientation == "horizontal") {
                if ((previousY - y == 1 || previousY - y == -1) && (previousX - x == 0)) {
                    shipCoords += ":" + QString::number(x) + QString::number(y);
                    previousX = x;
                    previousY = y;
                    countSize -= 1;
                    return true;
                }
            }
        }
    }
    return false;
}

bool MainWindow::checkRadioClicked() {
    for (int i = 1; i <= 4; i++) {
        if (MainWindow::findChild<QRadioButton *>("radioButton" + QString::number(i))->isChecked()) {
            shipSize = i;
            MainWindow::findChild<QPushButton *>("cancelPushButton")->setEnabled(true);
            return true;
        }
    }
    return false;
}

void MainWindow::clearBoard(){

    for (int i = 0; i < 10; i++){
        for (int j = 0; j < 10; j++){
            board[i][j] = "";
        }
    }
}

void MainWindow::disableUncheckedRadioButtons() {

    for (int i = 1; i <= 4; i++) {
        MainWindow::findChild<QRadioButton *>("radioButton" + QString::number(i))->setEnabled(false);
    }
    MainWindow::findChild<QRadioButton *>("horizontalRadioButton")->setEnabled(false);
    MainWindow::findChild<QRadioButton *>("verticalRadioButton")->setEnabled(false);
}

void MainWindow::enableRadioButtons() {

    for (int i = 1; i <= 4; i++)
        MainWindow::findChild<QRadioButton *>("radioButton" + QString::number(i))->setEnabled(true);
    MainWindow::findChild<QRadioButton *>("horizontalRadioButton")->setEnabled(true);
    MainWindow::findChild<QRadioButton *>("verticalRadioButton")->setEnabled(true);
}

void MainWindow::lockNeighbourFields() {

    allShipsPositions += ":" + shipCoords;
    QStringList lockList = shipCoords.split(":");
    QString firstElement = lockList.first();
    QString lastElement = lockList.last();
    addShipCount();
    MainWindow::findChild<QPushButton *>("cancelPushButton")->setEnabled(false);

    if (shipSize == 1) {

        if (firstElement.left(1) != "9")
            prepareButtons[firstElement.left(1).toInt() + 1][firstElement.right(1).toInt()]->setEnabled(false);
        if (firstElement.left(1) != "9" && firstElement.right(1) != "9")
            prepareButtons[firstElement.left(1).toInt() + 1][firstElement.right(1).toInt() + 1]->setEnabled(false);
        if (firstElement.left(1) != "9" && firstElement.right(1) != "0")
            prepareButtons[firstElement.left(1).toInt() + 1][firstElement.right(1).toInt() - 1]->setEnabled(false);

        if (firstElement.left(1) != "0")
            prepareButtons[firstElement.left(1).toInt() - 1][firstElement.right(1).toInt()]->setEnabled(false);
        if (firstElement.left(1) != "0" && firstElement.right(1) != "0")
            prepareButtons[firstElement.left(1).toInt() - 1][firstElement.right(1).toInt() - 1]->setEnabled(false);
        if (firstElement.left(1) != "0" && firstElement.right(1) != "9")
            prepareButtons[firstElement.left(1).toInt() - 1][firstElement.right(1).toInt() + 1]->setEnabled(false);

        if (firstElement.right(1) != "9")
            prepareButtons[firstElement.left(1).toInt()][firstElement.right(1).toInt() + 1]->setEnabled(false);
        if (firstElement.right(1) != "9" && firstElement.left(1) != "9")
            prepareButtons[firstElement.left(1).toInt() + 1][firstElement.right(1).toInt() + 1]->setEnabled(false);
        if (firstElement.right(1) != "9" && firstElement.left(1) != "0")
            prepareButtons[firstElement.left(1).toInt() - 1][firstElement.right(1).toInt() + 1]->setEnabled(false);

        if (firstElement.right(1) != "0")
            prepareButtons[firstElement.left(1).toInt()][firstElement.right(1).toInt() - 1]->setEnabled(false);
        if (firstElement.right(1) != "0" && firstElement.right(1) != "0")
            prepareButtons[firstElement.left(1).toInt() - 1][firstElement.right(1).toInt() - 1]->setEnabled(false);
        if (firstElement.right(1) != "0" && firstElement.right(1) != "9")
            prepareButtons[firstElement.left(1).toInt() - 1][firstElement.right(1).toInt() + 1]->setEnabled(false);

    } else {

        if (shipOrientation == "vertical") {
            lockNeighbourFieldsVertical(lockList, firstElement, lastElement);
        } else if (shipOrientation == "horizontal") {
            lockNeighbourFieldsHorizontal(lockList, firstElement, lastElement);
        }
    }
}

void MainWindow::lockNeighbourFieldsVertical(QStringList lockList, QString firstElement, QString lastElement) {

    if (board[firstElement.left(1).toInt() - 1][firstElement.right(1).toInt()] != "") {

        if (firstElement.left(1) != "9")
            prepareButtons[firstElement.left(1).toInt() + 1][firstElement.right(1).toInt()]->setEnabled(false);

        if (firstElement.left(1) != "9" && firstElement.right(1) != "9")
            prepareButtons[firstElement.left(1).toInt() + 1][firstElement.right(1).toInt() + 1]->setEnabled(false);

        if (firstElement.left(1) != "9" && firstElement.right(1) != "0")
            prepareButtons[firstElement.left(1).toInt() + 1][firstElement.right(1).toInt() - 1]->setEnabled(false);

        for (int i = 0; i < shipSize; i++) {

            if (lockList[i].right(1) != "9")
                prepareButtons[lockList[i].left(1).toInt()][lockList[i].right(1).toInt() + 1]->setEnabled(false);
            if (lockList[i].right(1) != "0")
                prepareButtons[lockList[i].left(1).toInt()][lockList[i].right(1).toInt() - 1]->setEnabled(false);
        }

        if (lastElement.left(1) != "0")
            prepareButtons[lastElement.left(1).toInt() - 1][lastElement.right(1).toInt()]->setEnabled(false);

        if (lastElement.left(1) != "0" && lastElement.right(1) != "0")
            prepareButtons[lastElement.left(1).toInt() - 1][lastElement.right(1).toInt() - 1]->setEnabled(false);

        if (lastElement.left(1) != "0" && lastElement.right(1) != "9")
            prepareButtons[lastElement.left(1).toInt() - 1][lastElement.right(1).toInt() + 1]->setEnabled(false);

    } else {

        if (firstElement.left(1) != "0")
            prepareButtons[firstElement.left(1).toInt() - 1][firstElement.right(1).toInt()]->setEnabled(false);

        if (firstElement.left(1) != "0" && firstElement.right(1) != "9")
            prepareButtons[firstElement.left(1).toInt() - 1][firstElement.right(1).toInt() + 1]->setEnabled(false);

        if (firstElement.left(1) != "0" && firstElement.right(1) != "0")
            prepareButtons[firstElement.left(1).toInt() - 1][firstElement.right(1).toInt() - 1]->setEnabled(false);

        for (int i = 0; i < shipSize; i++) {

            if (lockList[i].right(1) != "0")
                prepareButtons[lockList[i].left(1).toInt()][lockList[i].right(1).toInt() - 1]->setEnabled(false);
            if (lockList[i].right(1) != "9")
                prepareButtons[lockList[i].left(1).toInt()][lockList[i].right(1).toInt() + 1]->setEnabled(false);
        }
        if (lastElement.left(1) != "9")
            prepareButtons[lastElement.left(1).toInt() + 1][lastElement.right(1).toInt()]->setEnabled(false);

        if (lastElement.left(1) != "9" && lastElement.right(1) != "9")
            prepareButtons[lastElement.left(1).toInt() + 1][lastElement.right(1).toInt() + 1]->setEnabled(false);

        if (lastElement.left(1) != "9" && lastElement.right(1) != "0")
            prepareButtons[lastElement.left(1).toInt() + 1][lastElement.right(1).toInt() - 1]->setEnabled(false);
    }
}

void MainWindow::lockNeighbourFieldsHorizontal(QStringList lockList, QString firstElement, QString lastElement) {

    if (board[firstElement.left(1).toInt()][firstElement.right(1).toInt() + 1] != "") {

        if (firstElement.right(1) != "0")
            prepareButtons[firstElement.left(1).toInt()][firstElement.right(1).toInt() - 1]->setEnabled(false);

        if (firstElement.right(1) != "0" && firstElement.left(1) != "0")
            prepareButtons[firstElement.left(1).toInt() - 1][firstElement.right(1).toInt() - 1]->setEnabled(false);

        if (firstElement.right(1) != "0" && firstElement.left(1) != "9")
            prepareButtons[firstElement.left(1).toInt() + 1][firstElement.right(1).toInt() - 1]->setEnabled(false);

        for (int i = 0; i < shipSize; i++) {

            if (lockList[i].left(1) != "9")
                prepareButtons[lockList[i].left(1).toInt() + 1][lockList[i].right(1).toInt()]->setEnabled(false);
            if (lockList[i].left(1) != "0")
                prepareButtons[lockList[i].left(1).toInt() - 1][lockList[i].right(1).toInt()]->setEnabled(false);
        }

        if (lastElement.right(1) != "9")
            prepareButtons[lastElement.left(1).toInt()][lastElement.right(1).toInt() + 1]->setEnabled(false);

        if (lastElement.right(1) != "9" && lastElement.left(1) != "0")
            prepareButtons[lastElement.left(1).toInt() - 1][lastElement.right(1).toInt() + 1]->setEnabled(false);

        if (lastElement.right(1) != "9" && lastElement.left(1) != "9")
            prepareButtons[lastElement.left(1).toInt() + 1][lastElement.right(1).toInt() + 1]->setEnabled(false);

    } else {

        if (firstElement.right(1) != "9")
            prepareButtons[firstElement.left(1).toInt()][firstElement.right(1).toInt() + 1]->setEnabled(false);

        if (firstElement.right(1) != "9" && firstElement.left(1) != "0")
            prepareButtons[firstElement.left(1).toInt() - 1][firstElement.right(1).toInt() + 1]->setEnabled(false);

        if (firstElement.right(1) != "9" && firstElement.left(1) != "9")
            prepareButtons[firstElement.left(1).toInt() + 1][firstElement.right(1).toInt() + 1]->setEnabled(false);

        for (int i = 0; i < shipSize; i++) {

            if (lockList[i].left(1) != "9")
                prepareButtons[lockList[i].left(1).toInt() + 1][lockList[i].right(1).toInt()]->setEnabled(false);
            if (lockList[i].left(1) != "0")
                prepareButtons[lockList[i].left(1).toInt() - 1][lockList[i].right(1).toInt()]->setEnabled(false);
        }
        if (lastElement.right(1) != "0")
            prepareButtons[lastElement.left(1).toInt()][lastElement.right(1).toInt() - 1]->setEnabled(false);

        if (lastElement.right(1) != "0" && lastElement.left(1) != "9")
            prepareButtons[lastElement.left(1).toInt() + 1][lastElement.right(1).toInt() - 1]->setEnabled(false);

        if (lastElement.right(1) != "0" && lastElement.left(1) != "0")
            prepareButtons[lastElement.left(1).toInt() - 1][lastElement.right(1).toInt() - 1]->setEnabled(false);
    }
}

bool MainWindow::checkAmountOfShips() {

    switch(shipSize) {

    case 1:
        if (ship1Count <= 4)
            return true;
        break;

    case 2:
        if (ship2Count <= 3)
            return true;
        break;

    case 3:
        if(ship3Count <= 2)
            return true;
        break;

    case 4:
        if(ship4Count <= 1)
            return true;
        break;
    }
    return false;
}

void MainWindow::addShipCount() {

    if (MainWindow::findChild<QRadioButton *>("radioButton1")->isChecked())
        ship1Count += 1;
    else if (MainWindow::findChild<QRadioButton *>("radioButton2")->isChecked())
        ship2Count += 1;
    else if (MainWindow::findChild<QRadioButton *>("radioButton3")->isChecked())
        ship3Count += 1;
    else if (MainWindow::findChild<QRadioButton *>("radioButton4")->isChecked())
        ship4Count += 1;
}

void MainWindow::cancelShipSetting() {

    QStringList lockList = shipCoords.split(":");

    for (int i = 0; i < clickedButtons; i++) {

        board[lockList[i].left(1).toInt()][lockList[i].right(1).toInt()] = "";
        prepareButtons[lockList[i].left(1).toInt()][lockList[i].right(1).toInt()]->setStyleSheet("background-color: #FFFFFF;border: 1px solid black;");
        prepareButtons[lockList[i].left(1).toInt()][lockList[i].right(1).toInt()]->setEnabled(true);
    }
    clickedButtons = 0;
}

void MainWindow::receivedSomething(QString msg) {

    serverMessage = msg;
    QLabel *label = MainWindow::findChild<QLabel *>("label");
    QLabel *playerLabel = MainWindow::findChild<QLabel *>("playerLabel");
    QLabel *opponentLabel = MainWindow::findChild<QLabel *>("opponentLabel");
    if (msg.contains("ready")) {
        QStringList list = msg.split(":");

        allShipsPositions = QString("ShipCoords") + ":" + allShipsPositions;
        sendMessage(allShipsPositions);
        allShipsPositions = "";
        shipCoords = "";
        if (list.last() == "y") {
            label->setText("Your turn");
            label->setStyleSheet("color: rgb(0, 255, 0); font-size: 20px; font-weight: bold;");
        } else {
            label->setText("Oponnent's turn");
            label->setStyleSheet("color: rgb(30, 144, 255); font-size: 20px; font-weight: bold;");
        }
        MainWindow::findChild<QWidget *>("page2")->setEnabled(true);
    }

    if (msg.contains("hit")) {

        if (msg.contains("enemy")) {
            QString str = msg.remove(0,8);
            int row = str.left(1).toInt();
            int column = str.right(1).toInt();
            allButtons[row][column]->setStyleSheet("background-color: black; border: 1px solid black; color: white");
            allButtons[row][column]->setText("X");
        } else {
            QString str = msg.remove(0,3);
            int row = str.left(1).toInt();
            int column = str.right(1).toInt();
            enemyButtons[row][column]->setStyleSheet("background-color: red; border: 1px solid black;");
            enemyButtons[row][column]->setEnabled(false);
        }
    }

    if (msg.contains("miss")) {

        if (msg.contains("enemy")) {
            QString str = msg.remove(0,9);
            int row = str.left(1).toInt();
            int column = str.right(1).toInt();
            allButtons[row][column]->setText("X");
            allButtons[row][column]->setStyleSheet("background-color: white; border: 1px solid black; color: black");
            label->setText("Your turn");
            label->setStyleSheet("color: rgb(0, 255, 0); font-size: 20px; font-weight: bold;");
        } else {
            QString str = msg.remove(0,4);
            int row = str.left(1).toInt();
            int column = str.right(1).toInt();
            enemyButtons[row][column]->setText("X");
            enemyButtons[row][column]->setStyleSheet("background-color: white; border: 1px solid black; color: black");
            enemyButtons[row][column]->setEnabled(false);
            label->setText("Opponent's turn");
            label->setStyleSheet("color: rgb(30, 144, 255); font-size: 20px; font-weight: bold;");
        }
    }

    if (msg == "win") {
        QMessageBox::StandardButton reply = QMessageBox::question(this, "Win", "You have win, play revenge?", QMessageBox::Yes | QMessageBox::No);
        if (reply == QMessageBox::Yes) {
            playerScore += 1;
            sendMessage("revenge");
        } else {
            playerScore += 1;
            resetGame();
        }
        playerLabel->setText(QString::number(playerScore));
    }

    if (msg == "lose") {
        QMessageBox::StandardButton reply = QMessageBox::question(this, "Lose", "You have lose, play revenge?", QMessageBox::Yes | QMessageBox::No);
        if (reply == QMessageBox::Yes) {
            opponentScore += 1;
            sendMessage("revenge");
        } else {
            resetGame();
            opponentScore = 0;
        }
        opponentLabel->setText(QString::number(opponentScore));
    }

    if (msg == "reset") {
        if (label->text() == "Your turn")
            label->setText("Opponent's turn");
        else {
            label->setText("Your turn");
        }
        resetGame();
    }


}

void MainWindow::gotError(QAbstractSocket::SocketError err)
{
    //qDebug() << "got error";
    QString strError = "unknown";
    switch (err)
    {
    case 0:
        strError = "Connection was refused";
        break;
    case 1:
        strError = "Remote host closed the connection";
        break;
    case 2:
        strError = "Host address was not found";
        break;
    case 5:
        strError = "Connection timed out";
        break;
    default:
        strError = "Unknown error";
    }

    QMessageBox::warning(this, "Connection error", strError);
    resetGame();
}

void MainWindow::sendMessage(QString str) {
    QByteArray arrBlock;
    QDataStream out(&arrBlock, QIODevice::WriteOnly);
    out << quint16(0) << str;

    out.device()->seek(0);
    out << quint16(arrBlock.size() - sizeof(quint16));

    client->tcpSocket->write(arrBlock);
}

void MainWindow::resetGame() {

    clearBoard();
    for (int i = 0; i < 10; i++) {
        for (int j = 0; j < 10; j++) {
            allButtons[i][j]->setStyleSheet("background-color: white; border: 1px solid black; color: black");
            allButtons[i][j]->setText("");
            enemyButtons[i][j]->setStyleSheet("background-color: white; border: 1px solid black");
            enemyButtons[i][j]->setText("");
            enemyButtons[i][j]->setEnabled(false);
            prepareButtons[i][j]->setStyleSheet("background-color: white; border: 1px solid black;");
            prepareButtons[i][j]->setText("");
            prepareButtons[i][j]->setEnabled(true);

        }
    }
    MainWindow::findChild<QStackedWidget *>("stackedWidget")->setCurrentIndex(0);
}

void MainWindow::clearAllBoard() {

    for (int i = 0; i < 10; i ++) {
        for (int j = 0; j < 10; j++) {
            prepareButtons[i][j]->setStyleSheet("background-color: white; border: 1px solid black;");
            prepareButtons[i][j]->setText("");
            prepareButtons[i][j]->setEnabled(true);
        }
    }
    enableRadioButtons();
    clearBoard();
}

void MainWindow::on_radioButton4_clicked()
{
    countSize = 4;
}

void MainWindow::on_radioButton3_clicked()
{
    countSize = 3;
}

void MainWindow::on_radioButton2_clicked()
{
    countSize = 2;
}

void MainWindow::on_radioButton1_clicked()
{
    countSize = 1;
}



void MainWindow::on_verticalRadioButton_clicked() {
    countSize = shipSize;
    shipOrientation = "vertical";
    enableRadioButtons();
}

void MainWindow::on_horizontalRadioButton_clicked() {
    countSize = shipSize;
    shipOrientation = "horizontal";
    enableRadioButtons();
}

void MainWindow::on_cancelPushButton_clicked() {
    enableRadioButtons();
    QPushButton *button = (QPushButton *)sender();
    cancelShipSetting();
    button->setEnabled(false);
}

void MainWindow::on_startGamePushButton_clicked()
{
    //    if (amountOfShips == 10) {
    if (client == NULL) {
        ship4Count = 1;
        ship3Count = 1;
        ship2Count = 1;
        ship1Count = 1;
        clickedButtons = 0;
        amountOfShips = 0;
        establishConnection();
        client->connect2host();
    } else {
        sendMessage("ready" + allShipsPositions);
        ship4Count = 1;
        ship3Count = 1;
        ship2Count = 1;
        ship1Count = 1;
        clickedButtons = 0;
        amountOfShips = 0;
        MainWindow::findChild<QStackedWidget *>("stackedWidget")->setCurrentIndex(1);
        for (int i = 0; i < 10; i++)
            for (int j = 0; j < 10; j++)
                enemyButtons[i][j]->setEnabled(true);
    }
    //    } else {
    //        QMessageBox::warning(this, "Warning", "Not enough ships!");
    //    }
}

void MainWindow::myButtonPressed() {

}

void MainWindow::on_pushButton_clicked()
{
    client->closeConnection();
    MainWindow::findChild<QStackedWidget *>("stackedWidget")->setCurrentIndex(0);
}

void MainWindow::on_clearAllButton_clicked()
{
    clearAllBoard();
    ship4Count = 1;
    ship3Count = 1;
    ship2Count = 1;
    ship1Count = 1;
    clickedButtons = 0;
    amountOfShips = 0;
    allShipsPositions = "";
    shipCoords = "";
}
